const h1El = document.querySelector('h1')

const videoEl = `
<video autoplay muted loop>
  <source src="assets/warp_repix.mp4" type="video/mp4">
</video>
<audio autoplay loop>
  <source src="assets/warp_OST.mp3" type="audio/mpeg">
</audio>
`

export const enable = () => {
  h1El.innerText = 'Quantum ' + h1El.innerText
  h1El.className = 'quantum'
  document.getElementById('quantumWarp').innerHTML = videoEl
  document.querySelector('main').className = 'quantum'
}

export const disable = () => {
  h1El.innerText = h1El.innerText.replace('Quantum ', '')
  h1El.className = ''
  document.getElementById('quantumWarp').innerHTML = ''
  document.querySelector('main').className = ''
}
