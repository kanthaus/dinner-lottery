import { formatNames, isAssignabe } from './utils.mjs'

export const getParticipants = () => {
  const participants = document.getElementsByName('participants')[0].value
    .replace(/example:?/i, '')
    .trim()

  return formatNames(participants).filter(isAssignabe)
}

const dayPattern = /^\s*-\s*(\w+).*:(.*)$/
export const getSectionDaysData = sectionId => {
  const section = document.getElementsByName(sectionId)[0].value
  const sectionData = {}
  section.split('\n').forEach(line => {
    if (!dayPattern.test(line)) return
    const [ , day, names ] = line.match(dayPattern)
    sectionData[day.toLowerCase().slice(0, 3)] = formatNames(names)
  })
  return sectionData
}
