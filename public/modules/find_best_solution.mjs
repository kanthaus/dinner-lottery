import { remove, clone, info, tasksCodes } from './utils.mjs'
import { boringShuffleArray, quantumShuffleArray } from './shuffle_array.mjs'

export default async input => {
  let attempt = 0
  let bestSolution = { score: -Infinity }
  while (attempt++ < window.maxAttempts - 1) {
    const solution = await generateSolution(input)
    solution.attempt = attempt
    info('======== attempt', attempt, JSON.stringify(solution, null, 2))
    if (solution.score >= input.maxScore) return solution
    else if (!bestSolution || solution.score > bestSolution.score) bestSolution = solution
  }
  bestSolution.totalAttempts = attempt
  return bestSolution
}

let reported
const generateSolution = async ({ participants, volunteers, unavailabilities, maxScore }) => {
  const shuffleArray = window.quantumMode ? quantumShuffleArray : boringShuffleArray

  participants = await shuffleArray(participants)
  // Reset at each generation
  reported = []

  const weekPlan = clone(volunteers)
  const shuffledParticipants = clone(participants)

  let pass = 0
  while (++pass < 3) {
    const shuffledTasksCodes = await shuffleArray(tasksCodes)
    shuffledTasksCodes.forEach(day => addDayWorker(weekPlan, day, participants, unavailabilities, pass))
  }

  weekPlan.score = getWeekPlanScore(weekPlan)
  weekPlan.maxScore = maxScore
  weekPlan.nonAssigned = reported.concat(participants, unavailabilities.wee)
  weekPlan.shuffledParticipants = shuffledParticipants
  return weekPlan
}

const addDayWorker = (weekPlan, day, participants, unavailabilities, pass) => {
  info(`[${day}]`, '------------------------------', JSON.stringify({ participants, reported }, null, 2))
  weekPlan[day] = weekPlan[day] || []
  unavailabilities[day] = unavailabilities[day] || []
  if (weekPlan[day].length >= pass) return

  const foundCandidate = findCandidate(participants, unavailabilities, day)
  if (foundCandidate) weekPlan[day].push(foundCandidate)
}

const findCandidate = (participants, unavailabilities, day) => {
  let foundCandidate = canOnDay(reported, unavailabilities[day])[0]
  if (foundCandidate) {
    remove(reported, foundCandidate)
    info(`[${day}]`, foundCandidate, 'can (first from reported that can)')
    return foundCandidate
  } else {
    while (participants.length > 0) {
      const nextParticipant = participants.shift()
      if (unavailabilities[day].includes(nextParticipant)) {
        info(`[${day}]`, nextParticipant, 'can NOT')
        reported.push(nextParticipant)
      } else {
        info(`[${day}]`, nextParticipant, 'can')
        remove(participants, nextParticipant)
        return nextParticipant
      }
    }
  }
}

const canOnDay = (names, dayUnavailabilities) => names.filter(name => !dayUnavailabilities.includes(name))

const getWeekPlanScore = weekPlan => {
  let score = 0
  Object.values(weekPlan).forEach(workers => score += workers.length)
  Object.values(weekPlan).forEach(workers => {
    if (workers.length < 2) score -= (2 - workers.length)**2
  })
  return score
}