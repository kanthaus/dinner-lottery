import { tasksCodes } from './utils.mjs'

export default solution => {
  const { lotteryParticipants, nonAssigned } = solution
  const cells = tasksCodes
    .map(day => `<td>${solution[day].join(', ')}</td>`)
    .join('')

  return `
  <h2>Results</h2>

  <table>
    <thead>
      <tr>
        <td>Monday</td>
        <td>Tuesday</td>
        <td>Wednesday</td>
        <td>Thursday</td>
        <td>Friday</td>
        <td>Open Tuesday</td>
      </tr>
    </thead>
    <tbody>
      <tr>${cells}</tr>
    </tbody>
  </table>

  <p><strong>Lottery participants:</strong> ${lotteryParticipants.join(', ')}</strong></p>
  <p><strong>Non-assigned:</strong> ${nonAssigned.join(', ')}</strong></p>
  `
}
