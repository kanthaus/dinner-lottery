const shuffleArray = (randomFn, prepareFn) => async array => {
  if (prepareFn) await prepareFn(array.length)
  return array
  .map(addRandomScore(randomFn))
  .sort(byScore)
  .map(getName)
}

const addRandomScore = randomFn => name => ({ name, score: randomFn() })

// See Array.sort documentation https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
const byScore = (a, b) => b.score - a.score
const getName = ({ name }) => name

const getBoringRandomNumber = () => Math.random()

const quantumReservoir = []

const refillQuantumReservoir = async length => {
  if (length === 0) return
  const url = `https://qrng.anu.edu.au/API/jsonI.php?length=${length}&type=uint16`
  const { data } = await fetch(url).then(res => res.json())
  quantumReservoir.push(...data)
}

const getQuantumRandomNumber = () => {
  if (quantumReservoir.length === 0) alert('QUANTUM DOOM')
  else return quantumReservoir.pop()
}

export const boringShuffleArray = shuffleArray(getBoringRandomNumber)
export const quantumShuffleArray = shuffleArray(getQuantumRandomNumber, refillQuantumReservoir)
