const lotteryHeaderPattern = /###.{1,20}lottery/i
const headerPattern = /^\s*#* /

export default async (options = {}) => {
  const { overwrite = true } = options
  const text = await fetch('https://codi.kanthaus.online/come/download').then(res => res.text())
  const { participants, volunteers, unavailabilities } = parse(text)
  console.log({ participants, volunteers, unavailabilities })

  set('participants', participants, overwrite)
  set('volunteers', volunteers, overwrite)
  set('unavailabilities', unavailabilities, overwrite)
}

const parse = text => {
  const data = {
    participants: '',
    volunteers: '',
    unavailabilities: '',
  }

  if (text.split('- Present:')[1]) {
    data.participants = text.split('- Present:')[1].split('\n')[0]
  }

  if (text.split(lotteryHeaderPattern)[1]){
    const lines = text
      .split(lotteryHeaderPattern)[1]
      .split('\n')

    let section

    for (let line of lines) {
      line = cleanupLine(line)
      if (line.match(headerPattern)) section = findSection(line)
      else if (section != null && data[section] != null) data[section] += line + '\n'
    }
  }

  return data
}

const set = (name, padValue, overwrite) => {
  const el = document.getElementsByName(name)[0]
  const nonEmptyElValue = el.value !== ''
  const nonEmptyPadValue = padValue.trim().length > 0
  const normalizedName = name.padEnd(17, ' ')
  if (overwrite || (!nonEmptyElValue && nonEmptyPadValue)) {
    el.value = padValue
    addStatus(`${new Date().toLocaleTimeString()}: set ${normalizedName} value from pad`)
  } else {
    const reason = nonEmptyElValue ? 'value already set' : 'empty pad value'
    addStatus(`${new Date().toLocaleTimeString()}: ${normalizedName} not set: ${reason}`)
  }
}

const statusEl = document.getElementById('status')
const addStatus = text => statusEl.innerHTML = `<p>${text}</p>` + statusEl.innerHTML

const cleanupLine = line => line.replace(/\*+/g, '')

const findSection = headerLine => {
  if (headerLine.toLowerCase().includes('vol')) return 'volunteers'
  else if (headerLine.toLowerCase().includes('unavail')) return 'unavailabilities'
  else return null
}
