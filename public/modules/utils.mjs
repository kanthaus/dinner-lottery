export const formatNames = str => {
  return str
  .split(nameSeparator)
  .map(format)
  .filter(isntEmpty)
}
const nameSeparator = /[\n,]+/
const format = name => {
  return name
  ?.trim()
  .toLowerCase()
  .replace(/^tais$/, 'taïs')
}
const isntEmpty = name => name !== ''

export const isAssignabe = name => !nonAssignable.includes(name.toLowerCase())
const nonAssignable = [ 'mika' ]

export const remove = (arrayA, arrayB) => {
  arrayB = forceArray(arrayB)
  arrayB.forEach(el => {
    const index = arrayA.indexOf(el)
    if (index > -1) arrayA.splice(index, 1)
  })
}

const forceArray = value => {
  if (value instanceof Array) return value
  else return [ value ]
}

export const clone = obj => JSON.parse(JSON.stringify(obj))

export const info = (...args) => {
  if (window.verbose) console.log(...args)
}

export const tasksCodes = [ 'mon', 'tue', 'wed', 'thu', 'fri', 'ope' ]
