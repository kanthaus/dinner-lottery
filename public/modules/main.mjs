import { getParticipants, getSectionDaysData } from './extract_input.mjs'
import findBestSolution from './find_best_solution.mjs'
import render from './render.mjs'
import { tasksCodes, remove } from './utils.mjs'
import { enable as enableQuantumWarp, disable as disableQuantumWarp } from './quantum_warp.mjs'
import importDataFromPad from './import_data_from_pad.mjs'

// Configurable from the console
window.maxAttempts = 100
window.verbose = false

window.addEventListener('DOMContentLoaded', async () => {
  await importDataFromPad({ overwrite: false })
  document.getElementById('generate').addEventListener('click', normalGenerate)
  document.getElementById('quantum').addEventListener('click', quantumGenerate)
  document.getElementById('reimport').addEventListener('click', importDataFromPad)
})

const normalGenerate = () => {
  window.quantumMode = false
  disableQuantumWarp()
  generate()
}

const quantumGenerate = async () => {
  if (!window.quantumMode) {
    window.quantumMode = true
    enableQuantumWarp()
  }
  generate()
}


const generate = async () => {
  const participants = getParticipants()
  const volunteers = getSectionDaysData('volunteers')
  const unavailabilities = getSectionDaysData('unavailabilities')
  const volunteersNames = [].concat(...Object.values(volunteers))
  // Remove week ('wee') unavailabilities
  if (unavailabilities.wee) remove(participants, unavailabilities.wee)
  // Save possibleWorkersCount before removing volunteers from the list
  const possibleWorkersCount = participants.length
  if (volunteersNames.length > 0) remove(participants, volunteersNames)

  const bestSolutionFound = await findBestSolution({
    participants,
    volunteers,
    unavailabilities,
    maxScore: getMaxPossibleScore(possibleWorkersCount)
  })

  bestSolutionFound.lotteryParticipants = participants

  console.log('best solution found', JSON.stringify(bestSolutionFound, null, 2))

  document.getElementById('results').innerHTML = render(bestSolutionFound)
}

const slots = tasksCodes.length * 2
const getMaxPossibleScore = possibleWorkersCount => {
  if (possibleWorkersCount >= slots) {
    return slots
  } else {
    const minimumEmptySlots = slots - possibleWorkersCount
    return possibleWorkersCount - minimumEmptySlots
  }
}
